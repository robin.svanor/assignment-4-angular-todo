# Angular Todo App

This application lets you add, remove and when a task is done you can click it to put a line through the text. It is an easy way to keep track of your daily tasks.

## Author

Robin Svanor