import { Component, OnInit } from '@angular/core';
import { Todo } from './../../models/todo.model';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todos: Todo [] = [];
  inputTodo : string = "";

  constructor() { }

  // Initial todos so the app won't be blank on startup
  ngOnInit(): void {
    this.todos = [
      {
        todoContent: "Take out the trash",
        isCompleted: false
      },
      {
        todoContent: "Walk the doggo",
        isCompleted: false
      },
      {
        todoContent: "Create a todo list",
        isCompleted: true
      }
    ]
  }

  // Method for adding todo tasks
  addTodo() {
    this.todos.push({
      todoContent: this.inputTodo,
      isCompleted: false
    });
    this.inputTodo = "";
  }

  // Method for deleting todo tasks
  deleteTodo(id: number) {
    this.todos = this.todos.filter((value, item) => item !== id);
  }

  // Method for completing a todo task
  onToggleDone(id: number) {
    this.todos.map((value, item) => {
      if (item == id) value.isCompleted = !value.isCompleted;
        return value;
    });
  }
}