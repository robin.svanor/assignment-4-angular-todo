export interface Todo {
    todoContent: string;
    isCompleted: boolean;
}